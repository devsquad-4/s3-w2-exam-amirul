package jdbc.dao;

import jdbc.pojo.Customer;

public interface CustomerDAO {
	
	void insertRecord(Customer refCust);
	void deleteRecord(Customer refCust);
	void updateRecord(Customer refCust);
	void custLogin(Customer refCust);
	
}