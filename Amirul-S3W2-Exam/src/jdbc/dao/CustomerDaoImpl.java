package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import jdbc.pojo.Customer;
import utility.DBUtility;

public class CustomerDaoImpl implements CustomerDAO {

	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;

	@Override
	public void custLogin(Customer refCust) {

		// refConnection ==> this connection reference we can use for our SQL queries
		ResultSet rs = null;

		try {
			
			//1. Allocate a connection object
			refConnection = DBUtility.getConnection();

			//Getting the record in sql
			String sqlQuery = "SELECT customer_id,customer_password FROM customer WHERE customer_id=? and customer_password=?";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCust.getCustID());
			refPreparedStatement.setString(2, refCust.getCustPassword());

			rs = refPreparedStatement.executeQuery();

			while (rs.next()) {
				String customer_id = rs.getString("customer_id");
				String customer_password = rs.getString("customer_password");

				if (refCust.getCustID().equals(customer_id) && refCust.getCustPassword().equals(customer_password)) {
					System.out.println("User Authenticated");
					
				} else if (!(refCust.getCustID().equals(customer_id) || !refCust.getCustPassword().equals(customer_password))){
					System.out.println("Invalid Credentials");
				}
				break;
			}

		} catch (Exception e) {
			System.out.println("Exception Handled while getting record.");
		} finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}

	}

	@Override
	public void insertRecord(Customer refCust) {
		try {
			refConnection = DBUtility.getConnection();

			// mysql insert query
			String sqlQuery = "insert into customer(customer_id,customer_password) values(?,?)";

			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCust.getCustID());
			refPreparedStatement.setString(2, refCust.getCustPassword());

			// Approach 1
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been inserted successfully.");

			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");

		} catch (Exception e) {
			System.out.println("Exception Handled while insert record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void deleteRecord(Customer refUser) {
		try {
			refConnection = DBUtility.getConnection();

			// mysql insert query
			String sqlQuery = "delete from customer where customer_id=? and customer_password=?";

			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getCustID());
			refPreparedStatement.setString(2, refUser.getCustPassword());

			// Approach 1
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been deleted successfully.");

			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully deleted");

		} catch (Exception e) {
			System.out.println("Exception Handled while deleting record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void updateRecord(Customer refUser) {
		try {
			refConnection = DBUtility.getConnection();

			// mysql insert query
			String sqlQuery = "update user set user_password = ? where user_id = ?";

			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getCustPassword());
			refPreparedStatement.setString(2, refUser.getCustID());

			// Approach 1
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been updated successfully.");

			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully deleted");

		} catch (Exception e) {
			System.out.println("Exception Handled while updating record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

}
