package jdbc.service;

public interface CustomerService {
	void custInputInsertRecord();
	void custChoice();
	void deleteRecord();
	void updateRecord();
	void custLogin();
}
