package jdbc.service;

import java.util.Scanner;

import jdbc.dao.CustomerDAO;
import jdbc.dao.CustomerDaoImpl;
import jdbc.pojo.Customer;

public class CustomerServiceImple implements CustomerService {

	CustomerDAO refCustDAO;
	Scanner scannerRef;
	Customer refCust;

	@Override
	public void custInputInsertRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Customer ID : ");
		String custLoginID = scannerRef.next();

		System.out.println("Enter Password : ");
		String custPassword = scannerRef.next();

		refCust = new Customer();
		refCust.setCustID(custLoginID);
		refCust.setCustPassword(custPassword);

		refCustDAO = new CustomerDaoImpl();
		refCustDAO.insertRecord(refCust);
	}



	@Override
	public void custChoice() {
		System.out.println("Enter Choice");
		System.out.println("1: Insert New Record");
		System.out.println("2: Customer Login");
		System.out.println("3: Delete Record");
		System.out.println("4: Update Record");

		scannerRef = new Scanner(System.in);
		int choice = scannerRef.nextInt();
		switch (choice) {
		case 1:
			custInputInsertRecord();
			break;
		case 2:
			custLogin();
			break;
		case 3:
			deleteRecord();
			break;
		case 4:
			updateRecord();
			break;
		
		default:
			System.out.println("Option not found..");
			break;
		} // end of userChoice

	}

	@Override
	public void deleteRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Customer ID to delete : ");
		String custLoginID = scannerRef.next();

		System.out.println("Enter Password : ");
		String custPassword = scannerRef.next();

		refCust = new Customer();
		refCust.setCustID(custLoginID);
		refCust.setCustPassword(custPassword);

		refCustDAO = new CustomerDaoImpl();
		refCustDAO.deleteRecord(refCust);
	}

	@Override
	public void updateRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Customer ID to update : ");
		String custLoginID = scannerRef.next();

		System.out.println("Enter New Password : ");
		String newCustPassword = scannerRef.next();

		refCust = new Customer();
		refCust.setCustID(custLoginID);
		refCust.setCustPassword(newCustPassword);

		refCustDAO = new CustomerDaoImpl();
		refCustDAO.updateRecord(refCust);

	}

	@Override
	public void custLogin() {

		scannerRef = new Scanner(System.in);

		System.out.println("Enter Customer ID : ");
		String userLoginID = scannerRef.next();

		System.out.println("Enter Customer Password : ");
		String userPassword = scannerRef.next();

		refCust = new Customer();
		refCust.setCustID(userLoginID);
		refCust.setCustPassword(userPassword);

		refCustDAO = new CustomerDaoImpl();
		refCustDAO.custLogin(refCust);
	}

}
